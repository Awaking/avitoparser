#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime, date
from datetime import timedelta
import time
import locale

locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8') # the ru locale is installed

class City:
    NOVOSIBIRSK = "novosibirsk"
    BELGOROD = "belgorod"


class AvitoAdv:
    viewsCount = 0
    title = price = url = placementDate = updateDate = description =  ''

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "ID: %s, Title: %s, Price: %s, URL: %s, Placement Date: %s, Update Date: %s, Views: %d, Description: %s" \
               % (self.id, self.title, self.price, self.url, self.placementDate, self.updateDate, self.viewsCount, self.description)


class AvitoParser:

    def __init__(self, webdriver, searchPhrase, category = '', city = City.NOVOSIBIRSK, pages = [1], isDebug = False, isSingleItemMode = False):
        self.webdriver = webdriver
        self.city = city
        self.searchPhrase = searchPhrase
        self.category = category
        self.pages = pages
        self.isDebug = isDebug
        self.isSingleItemMode = isSingleItemMode
        if self.isDebug:
            print("Debug mode = On")
        if self.isSingleItemMode:
            print("Single item mode = On")

    def avitoAdvs(self):
        avitoAdvs = {}
        if self.isDebug:
            print("Pages array: ", self.pages)
        for page in self.pages:
            if self.isDebug:
                print("Loading page №%d: %s ..." % (page, self.URLForPage(page)))

            self.webdriver.get(self.URLForPage(page))

            if self.isDebug:
                print("Got page №%d: %s" % (page, driver.title))


            #Not found case
            try:
                self.webdriver.find_element(By.CSS_SELECTOR, ".nulus")
                if self.isDebug:
                    print("NOT FOUND page №%d: %s ..." % (page, self.URLForPage(page)))
                break
            except NoSuchElementException:
                None

            assert "Avito" in self.webdriver.title
            tableItems = self.webdriver.find_elements(By.CSS_SELECTOR,
                                                      ".item.item_table.clearfix.js-catalog-item-enum.c-b-0")

            for index, tableItem in enumerate(tableItems):
                id = tableItem.get_property("id")[1:]
                adv = AvitoAdv(int(id))
                title = tableItem.find_element(By.CSS_SELECTOR, ".item-description-title-link")
                adv.title = title.text
                adv.url = title.get_property("href")
                adv.price = tableItem.find_element(By.CSS_SELECTOR, ".about").text
                dateString = tableItem.find_element(By.CSS_SELECTOR, ".date.c-2").text
                date = self.dateFromString(dateString)
                adv.updateDate = date
                adv.placementDate = adv.updateDate #May be changed during deep scan via updateAvitoAdv()
                avitoAdvs[id] = adv

                if self.isDebug:
                    print("Object created: ", adv)

                if self.isSingleItemMode:
                    break

        return avitoAdvs

    def URLForPage(self, page):
        url = u'https://www.avito.ru/%s' % self.city
        if self.category:
            url = u'%s/%s' % (url, self.category)

        url = u'%s?p=%d' % (url, page)
        if self.searchPhrase:
            url = u'%s&q=%s' % (url, self.searchPhrase)


        return url

    def updateAvitoAdv(self, avitoAdv):
        assert avitoAdv.url

        if self.isDebug:
            print("Updating item for URL: ", avitoAdv.url, "...")

        self.webdriver.get(avitoAdv.url)

        if self.isDebug:
            print("Got page: ",driver.title)

        assert "Avito" in self.webdriver.title

        try:
            descriptionItem = driver.find_element(By.CSS_SELECTOR, ".item-description-text")
        except NoSuchElementException:
            try:
                descriptionItem = driver.find_element(By.CSS_SELECTOR, ".item-description-html")
            except NoSuchElementException:
                descriptionItem = None

        finally:
            if descriptionItem:
                avitoAdv.description = descriptionItem.find_element(By.TAG_NAME, "p").text
            else:
                avitoAdv.description = ""


        try:
            viewsStatLink = driver.find_element_by_css_selector(".js-show-stat.pseudo-link")
            viewsStatLink.click();
            try:
                statsContainer = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".item-stats__container")))
            finally:
                stringDate = statsContainer.find_element(By.CSS_SELECTOR, ".item-stats__date").text[24:]
                avitoAdv.placementDate = self.dateFromString(stringDate)
                legendSection = statsContainer.find_element(By.CSS_SELECTOR, ".item-stats-legend")
                avitoAdv.viewsCount = int(legendSection.find_element(By.TAG_NAME, "strong").text.replace(" ", ""))



        except NoSuchElementException:
            #Get views from plain text by cutting right parenthesis section "15 (+17)" -> "15"
            avitoAdv.viewsCount = int(driver.find_element(By.CSS_SELECTOR, ".title-info-views").text.split("(", 1)[0].replace(" ", ""))

        if self.isDebug:
            print("Object updated: ", avitoAdv, "\n----------------")


    def updateAvitoAdvs(self, avitoAdvs):
        for id, avitoAdv in avitoAdvs.items():
            self.updateAvitoAdv(avitoAdv)


    def dateFromString(self, dateString):
        dateComponents = dateString.split(" ")
        assert 1 < len(dateComponents) < 4
        if len(dateComponents) == 2:
            if "Сегодня" in dateString:
                baseDate = date.fromtimestamp(time.time())
            elif "Вчера" in dateString:
                baseDate = date.fromtimestamp(time.time()) - timedelta (1) #Today minus one day

            timeString = dateString.split(" ", 1)[1]
            actualDateTime = datetime.strptime(timeString, "%H:%M")
            actualDateTime = actualDateTime.replace(baseDate.year, baseDate.month, baseDate.day)
        else:
            try:
                actualDateTime = datetime.strptime(dateString, "%d %B %H:%M") #"4 Февраля 21:12"
                actualDateTime = actualDateTime.replace(year=datetime.now().year)

            except ValueError:
                actualDateTime = datetime.strptime(dateString, "%d %B %Y")  # "20 ноября 2016"

        return actualDateTime

#Sample - Full cycle
driver = webdriver.PhantomJS()
avitoParser = AvitoParser(driver, u'кольцо', category=u'chasy_i_ukrasheniya',  city = City.NOVOSIBIRSK, pages=[5], isDebug = True, isSingleItemMode=True)
avitoAdvs = avitoParser.avitoAdvs()
avitoParser.updateAvitoAdvs(avitoAdvs)

for id, avitoAdv in avitoAdvs.items():
    print (avitoAdv)

driver.quit()

'''
#Sample - Explicit URL
driver = webdriver.PhantomJS()
avitoParser = AvitoParser(driver, "", city = City.NOVOSIBIRSK, isDebug = True,)
advByURL = AvitoAdv(0)
advByURL.url = "https://www.avito.ru/novosibirsk/chasy_i_ukrasheniya/kole_s_serezhkami_921502346"
avitoParser.updateAvitoAdv(advByURL)
print (advByURL)

driver.quit()
'''