import sqlalchemy
print ("Версия SQLAlchemy:", sqlalchemy.__version__)
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

class User(object):
    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password

    def __repr__(self):
        return "<User('%s','%s', '%s')>" % (self.name, self.fullname, self.password)


engine = create_engine('sqlite:///:memory:', echo=True)

metadata = MetaData()
users_table = Table('users', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    Column('fullname', String),
    Column('password', String)
)

metadata.create_all(engine)

from sqlalchemy.orm import mapper  #достать "Отобразитель" из пакета с объектно-реляционной моделью
print (mapper(User, users_table))  # и отобразить. Передает класс User и нашу таблицу
user = User("Вася", "Василий", "qweasdzxc")
print (user)  #Напечатает <User('Вася', 'Василий', 'qweasdzxc'>
print (user.id)  #Напечатает None